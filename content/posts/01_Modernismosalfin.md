---
title: "01 Modernismos al fin"
date: 2020-05-13T17:09:16Z
author: "Tano"
draft: false
toc: false
images:
tags: ["Remington D.C."]
---
Ni Yo tengo la edad de el mundo, ni el mundo tiene tan poca experiencia.  
Pero si que mas de uno hemos leído escuchado la frase *desde que el mundo es mundo* sucede lo mismo. Dando de esa forma una suerte de irrefutabilidad al tema  en ciernes.

Por el poder inalienable que dan dos Bichos roncando a pata suelta, es que otorgo caracter de ley a lo antes escrito.
No por nada en especial, pero prefiero suponer que siempre es bueno apoyarse en una caña que si está rota: mejor que no se note. 

Se hace necesario entonces tener en cuenta la diferencia entre *definición* y *juicio*.  
Entendiendose al segundo como una afirmación, cosa que nunca es una *definición*. Dieron razón sobre *el juicio* los pensadores alegando que es la actividad intelectual que mas involucra al hombre. Es un verdadero despropósito que a *los razonamientos*  se les dedique tan poco tiempo

Dado que todo juicio es formalmente una afirmación, y también es un acto de *entendimiento*, presento aqui nada mas que *conceptos*, que como tales pueden ser representativos de objetos *reales* o *ideales*.

No hace mucho recordé una lectura sobre periodos de la historia. En esos escritos quedaron vestigios de usos y costumbres registrados como sucesiones rítmicas ejecutadas en manada. Esto si que es hacer historia.

Sobre esas secuencias es que baso a este y a los siguientes papiros.

Por leer esto no habrá suicidios en masa, tampoco alteración del magma terrestre, zamba de placas teutónicas o *impuestazos* (Mejor no hablar. De ciertas. Cosas).  

Comienzo citando veintena larga de conceptos que considero importantes, para luego sobre ellos desarrollar la idea:

 | |
 :-: | :-: | :-: | :-: | :-:
 Religión | Organización | Revolución | Estado | Política
 Razón | Mercado | Ciencia | Tecnología | Empleador
 Derechos | Liberal | Militar | Popular | Trabajador
 Lógica | Desigualdad | Productividad | Industria | Libertad
 Procesos | Individualismo | Voluntad | Metas | Control

Cuando el aspecto gráfico toma orden bajo esquema de mapa mental, suele tornarse un tanto mas expeditiva la explicación, veamos tanto las definiciones como las conexiones de cada concepto:

[![modern-raiz-contr.png](https://i.postimg.cc/zXw2vMtp/modern-raiz-contr.png)](https://postimg.cc/w7vQbWds)

[![modernidad-dosramas-despl.png](https://i.postimg.cc/hPsNCZVX/modernidad-dosramas-despl.png)](https://postimg.cc/nCsdrkRx)

La característica que quizá mejor defina a la modernidad es _el abandono de la religión_ como argumento explicativo para todo, siendo esta reemplazada por la ciencia y sus sólidas pruebas de campo. _La Razón_.

[![significantes-despl.png](https://i.postimg.cc/659PxDRL/significantes-despl.png)](https://postimg.cc/Jt2xbYpG)

La _modernidad_ sale a la cancha luego de la _revolución americana_ y la _francesa_, por lo que es de buen pensador adjudicarle como pesebre de natalicio a la _vecchia_ Europa para luego tomar posesión del planeta completo (vericuetos del éxito cuando arrasa con lo que encuentra).

Seguimos en [***este***](../02_modernismosalfin/) 2º escrito.
