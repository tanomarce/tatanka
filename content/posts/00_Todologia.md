---
title: "00 Todología"
author: "Tano"
date: 2020-05-02T20:25:31-03:00
draft: false
toc: false
images:
tags: ["Remington D.C."]
---
En plena era de las conexiones, cualquier Usuario con un elemento que le permita formar parte de la comunicación mediada puede convertirse en productor de sentido. De ahi la sobredosis de reversionados caseros sobre temas que mantuvieron ocupados durantes centenarios a los Pibes de la Greta Ateniense.

Sin un ápice de interés en esperar un tiempo de maduración como resultante del período reflexivo, el esquema *escucho-repito* se apoderó de los automáticos.  
Esto no es de ahora.  
Diez centímetros a la derecha o 20 a la izquierda, el Ser Humano *siempre avanzó* sin rumbo fijo manteniendo un zigzag que lo lleva a permanecer por tiempos indefinidos en ningún lugar.

Lo antes dicho no vendría a cuento de este escrito si no estuviese en escena un tercer axioma: el *todólogo* parlante.

Que un usuario de la red se convierta en *todólogo* es un sinsentido inofensivo en apariencia. Nada mas que en apariencia. No la misma categorización corresponde a la *trova mano de obra barata voz del amo* que la mass media incluyó en su dieta vacía de sentido. Una verdadera orda de Gente que se gana el mango aturdiendo a quién ronde cerca de un altoparlante.

Cuando los escuché las primeras veces me aburrieron. Zapatos en exposición y posturas del montón, las mismas que he visto cada vez que la manada despunta el vicio de huir de la soledad junto a un cierto número de parecidos. O reunión, si asi acostumbra a llamarla.

Luego, entendí que no había nada que entender. Esto: era, es y sera asi. La arena de el circo romano necesita sangre fresca. La impidadosa mass media cual Saturno, devorará a sus hijos recurriendo para ello a un mecanismo natural infalible: que el tiempo y el olvido ejecuten su obra maestra.

Tampoco _aparenta_ peligro para si mismo ni para terceros *la voz que no dice*. Bien escribo: no *aparenta*. Hay entidades atómicas sin brújula, carentes de tiempo luego de una jornada de trabajo sin una sola noticia alentadora, y a eso, la *fonóla sarasa* lo sabe muy bien. Reconoce ya sin tapujos que no importa *qué* diga, sino que lo repita hasta el cansancio. Lo avisó la Teoría Hipodérmica, el veneno satura en sangre por repetición.

Nunca una *fonóla* obligó a nadie a convertirse en lo que no estuviese de acuerdo. El mercado ya existía y nunca pudo permanecer un poco menos ansioso. Un *todólogo* ni es bueno ni es malo. Es: lo que aburre cuando se mira en esa dirección, lo que representa a la media de *consumidores*.

Nunca fue diferente.
