---
title: "02 Modernismos al fin"
date: 2020-06-05T02:39:21Z
draft: false
toc: false
images:
tags: ["Remington D.C."]
---
De [***aquí***](../01_modernismosalfin/) vinimos.  
Leímos que _La Modernidad_ tiene a sus orígines en Europa, hago una pausa ya que recordé
una frase de _Ernest Renán_ (escritor, filósofo, arqueólogo e historiador francés), dijo
Don Ernesto: 

    Las verdades que la ciencia revela superan siempre a los sueños que destruye.

Cuando las acciones, los signos y las cosas están liberados de la idea que los originó,
comienzan a autoreproducirse hasta el infinito.

Pensadores como Foucault, Lyotard, Lipovetsky (y Gran Elenco) han mostrado que una vez
rota la unidad y la dirección al que los sujetaba el progreso mesiánico, el Hombre se
encuentra en un laberinto sin saber cuánto avanza o cuánto retrocede.

Ideas:

* 
