---
title: "00 Habitante Pingüino"
date: 2020-04-29T18:21:34-03:00
author: "Tano"
draft: false
toc: false
images:
tags: ["GNU/Linux"]
---
# Índice
* [Palabras Reservadas](#palabras-reservadas)
* [Entrada en Calor](#entrada-calor)
* [Pasos Necesarios](#pasos-necesarios)
* [Tenga en cuenta que:](#tenga-cuenta)
* [Apoyo Final](#apoyo-final)

***
### Palabras reservadas de la entrada {#palabras-reservadas}
***Capa8:*** Usuario de PC.  
***Cacharra:*** Computadora portátil o de Escritorio.
***
### Entrada en Calor {#entrada-calor}

Ríos de letras han navegado monitores opinando sobre embarcaciones de bandera GNU/Linux.

 Según la experiencia *-nunca demasiada siempre necesaria-* dira el abatido decidor que dura ha sido la estancia en el astillero de instalación, pero dulce la travesía.

Hay escribientes diciendo de un determinado Sistema Operativo GNU/Linux movidos por la alegría o la frustración que deja haber arribado al éxito *o no*, que echan mano a epítectos en detrimento o casi ahogados en elogios.

Cada cual lleva su cuota de razón, ya que una opinión nace *-entre otras causas-* de la experiencia, y eso: es una de las pocas cuestiones irremplazables en este planeta.

No obstante, emito un pensamiento digno de quedar escrito *según lo veo*, y es referido a una condición tan humana como cualquiera de Nosotros: la manera de **pedir ayuda**.

- - -
### Pasos Necesarios {#pasos-necesarios}

Cualquier humanoide nacido de mujer que habiendo pagado sus impuestos, la boleta del suministro eléctrico y cuanta deuda acarrea, sin pesares su alma debería al menos tomarse algunos minutos de su vida para:

1. Haber leído el manual.
2. Buscar si hay soluciones en las secciones *Preguntas Frecuentes* (FAQ's) de los sitios referidos.
3. Haber llevado a cabo una búsqueda en la web.
4. Consultar a un conocido con experencia.

A la hora de pedir ayuda, conviene recordar que:
1. *Nadie tiene la varita mágica.*
2. *Los reyes mágicos son los padres.*

La adivinancia no hace rancho en las pampas binarias, no se puede ir por la vida diciendo:
>*¡Ayúdame no funciona!*

...sin otro motivo que el de salir rápido y sin esfuerzo de el trance, para ir a jugar con otro
Sistema Operativo, el circo de la desesperación no conmueve a nadie, todo
lo  contrario.

Nada en esta vida viene de regalo. Si se quiere lograr un objetivo: hay que trabajar. Nunca
alguien mintió al respecto.

**Es Necesario:**
1. Ser concreto respecto a la situación.
2. Aportar datos del entorno (Sistema Operativo y Escritorio como mínimo).
3. Confesar el/los pasos que se llevaron a cabo antes de la hecatombe.
4. Redactar lo mejor posible.
5. No mentir.

Hay algunas instancias mas que son importantes, no las menciono aquí porque estas son las
que considero relevantes. Cualquiera de estos cinco puntos (o los cinco a la vez) es lo
que muestra un *Capa8* caído en desgracia. Este mecanismo solicitante tiene un solo
veredicto: *no van por aquí los tiros*

---
### Tenga en cuentan que: {#tenga-cuenta}

1. Hay multitud de Usuarios para los que una computadora es una herramienta y nada mas, no
quieren saber vida, obra y milagros acerca de cómo está hecha, y está muy bien, es un
caso de uso mas, peero, ten en cuenta Estimad@ *Capa8* que en los huestes Pingüinas suele
haber mucha gente con perfil de Usuario Técnico, asi viven, asi ven la vida y, asi
van a responder.

2. No debería una Persona contraer matrimonio en primeras *náuseas* con el Che Guevara y
al otro día pedirle que se afeite...

3. El aterrizaje forzoso de un *Capa8* en zonas -que supone- van a resolverle la vida,
*siempre* va a encontrar una reacción directamente proporcional a la *nube
de pedos* donde viva.

4. Hay otros Sistemas Operativos, representan un caso de uso diferente. Muy, diferente. No es negocio usar *cacharras* con las que Uno no se siente cómodo.

5. Pero si alguien decide usar GNU/Linux, debe recordar que este, no es un Sistema para *expertos*, si: hecho por Expertos. Esta geografía puede albergar a cuanto Parroquiano quiera plantar cimientos, eso si:

    `es terreno inhóspito para gente vaga`.

___
### Apoyo Final {#apoyo-final}

Vaya mi apoyo incondicional a los *Capa8* que cuando se quiebran, no se quedan a vivir en
la grieta, los que ante un fallo no se rinden.

Ellos: son referentes.

[comment]: <> (![Usuario-Papúa](https://i.postimg.cc/mZcWF9wb/Us-Pap-transp.png))
![Usuario Papúa](https://i.postimg.cc/mZcWF9wb/Us-Pap-transp.png)
